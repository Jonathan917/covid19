import 'package:covid_app/model/status.dart';

import '../resources/repository.dart';
import 'package:geolocator/geolocator.dart';
import 'bloc_provider.dart';

class StatusBloc extends BlocBase {
  final _repository = Repository();

  Future<Information> getStatus(String isoCountry) async {
    final info = await _repository.getInfo(isoCountry);
    return info;
  }

  Future<String> getPosition() async {
    try {
      Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      List<Placemark> placemark = await Geolocator().placemarkFromCoordinates(position.latitude, position.longitude);
      return placemark[0].isoCountryCode;
    } catch (e) {
      print(e);
      return null;
    }
  }

  @override
  void dispose() {}
}
