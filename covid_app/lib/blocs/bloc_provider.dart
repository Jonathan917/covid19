import 'package:flutter/widgets.dart';

abstract class BlocBase {
  void dispose();
}

class BlocProvider<T> extends InheritedWidget {
  final T bloc;

  BlocProvider({Key key, @required Widget child, @required this.bloc}) : super(key: key, child: child);

  static T of<T extends BlocBase>(BuildContext context) {
    // final type = _typeOf<BlocProvider<T>>();
    return (context.dependOnInheritedWidgetOfExactType<BlocProvider>()).bloc;
  }

  // static Type _typeOf<T>() => T;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}
