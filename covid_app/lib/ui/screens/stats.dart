import 'package:covid_app/blocs/status_bloc.dart';
import 'package:covid_app/model/status.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart'; // for date format

class StatsScreen extends StatefulWidget {
  @override
  _StatsScreenState createState() => _StatsScreenState();
}

class _StatsScreenState extends State<StatsScreen> {
  StatusBloc _bloc;
  String _selectedButton = 'global';
  String _selectedTime = 'total';
  final now = DateTime.now();
  Information info;
  InformationStatus infoSpecify;
  List<LastCases> lastCases = [];

  @override
  Widget build(BuildContext context) {
    _bloc = Provider.of<StatusBloc>(context);
    return FutureBuilder<String>(
        future: _bloc.getPosition(),
        builder: (context, snapshot) {
          if (snapshot.data != null) {
            return FutureBuilder<Information>(
                future: _bloc.getStatus(snapshot.data),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    info = snapshot.data;
                    lastCases = _selectedButton == 'global' ? info.lastCasesGlobal : info.lastCasesCountry;
                    switch (_selectedTime) {
                      case 'today':
                        infoSpecify = _selectedButton == 'global' ? info.todayGlobal : info.todayCountry;
                        break;
                      case 'total':
                        infoSpecify = _selectedButton == 'global' ? info.totalGlobal : info.totalCountry;
                        break;
                      case 'yesterday':
                        infoSpecify = _selectedButton == 'global' ? info.yesterdayGlobal : info.yesterdayCountry;
                        break;
                      default:
                    }
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.white,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[_buildHeader(context), _buildChart()],
                        ),
                      ),
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                });
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  Container _buildChart() => Container(
        width: MediaQuery.of(context).size.width,
        // height: MediaQuery.of(context).size.height,
        color: Theme.of(context).primaryColor,
        child: Container(
          padding: EdgeInsets.only(top: 40.0, left: 25.0, right: 25.0),
          decoration: BoxDecoration(borderRadius: BorderRadius.only(topRight: Radius.circular(50), topLeft: Radius.circular(50)), color: Colors.white),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                'Daily New Cases',
                style: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0, bottom: 20.0),
                child: BarChart(
                  BarChartData(
                    alignment: BarChartAlignment.spaceAround,
                    maxY: (lastCases.fold(0, (previousValue, element) => previousValue + element.count) / lastCases.length) + 20000,
                    barTouchData: BarTouchData(
                      enabled: false,
                      touchTooltipData: BarTouchTooltipData(
                        tooltipBgColor: Colors.transparent,
                        tooltipPadding: const EdgeInsets.all(0),
                        tooltipBottomMargin: 8,
                        getTooltipItem: (
                          BarChartGroupData group,
                          int groupIndex,
                          BarChartRodData rod,
                          int rodIndex,
                        ) {
                          return BarTooltipItem(
                            rod.y.round().toString(),
                            TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                            ),
                          );
                        },
                      ),
                    ),
                    titlesData: FlTitlesData(
                      show: true,
                      bottomTitles: SideTitles(
                        showTitles: true,
                        textStyle: TextStyle(color: Colors.grey[400], fontWeight: FontWeight.bold, fontSize: 13),
                        margin: 20,
                        getTitles: (double value) {
                          switch (value.toInt()) {
                            case 0:
                              return DateFormat.MMMd().format(new DateTime(now.year, now.month, now.day - 6));
                            case 1:
                              return DateFormat.MMMd().format(new DateTime(now.year, now.month, now.day - 5));
                            case 2:
                              return DateFormat.MMMd().format(new DateTime(now.year, now.month, now.day - 4));
                            case 3:
                              return DateFormat.MMMd().format(new DateTime(now.year, now.month, now.day - 3));
                            case 4:
                              return DateFormat.MMMd().format(new DateTime(now.year, now.month, now.day - 2));
                            case 5:
                              return DateFormat.MMMd().format(new DateTime(now.year, now.month, now.day - 1));
                            case 6:
                              return DateFormat.MMMd().format(new DateTime(now.year, now.month, now.day));
                            default:
                              return '';
                          }
                        },
                      ),
                      leftTitles: SideTitles(
                        interval: (lastCases.fold(0, (previousValue, element) => previousValue + element.count) / lastCases.length) + 20000,
                        showTitles: true,
                        textStyle: TextStyle(color: Colors.grey[400], fontSize: 13),
                        reservedSize: 25,
                      ),
                    ),
                    borderData: FlBorderData(
                      show: false,
                    ),
                    barGroups: lastCases.map((e) => BarChartGroupData(x: 0, barRods: [BarChartRodData(y: e.count.toDouble(), color: Colors.red)])).toList(),
                  ),
                ),
              )
            ],
          ),
        ),
      );

  Container _buildHeader(BuildContext context) => Container(
        child: Container(
          color: Theme.of(context).primaryColor,
          padding: EdgeInsets.symmetric(horizontal: 25.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Text(
                  'Statistics',
                  style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
              ),
              DefaultTabController(
                length: 2,
                initialIndex: 1,
                child: Container(
                  // margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 6.0),
                  height: 55.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white24,
                    borderRadius: BorderRadius.circular(25.0),
                  ),
                  child: TabBar(
                    indicatorSize: TabBarIndicatorSize.tab,
                    indicator: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(25.0)),
                    labelStyle: TextStyle(fontSize: 17.0),
                    labelColor: Colors.black,
                    unselectedLabelColor: Colors.white,
                    tabs: <Widget>[
                      Text('My Country'),
                      Text('Global'),
                    ],
                    onTap: (index) {
                      if (index == 0) {
                        setState(() => {_selectedButton = 'local', infoSpecify = info.totalCountry});
                      } else {
                        setState(() => {_selectedButton = 'global', infoSpecify = info.totalGlobal});
                      }
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                        onPressed: () {
                          setState(() => {
                                _selectedTime = 'total',
                              });
                        },
                        child: Text(
                          'Total',
                          style: TextStyle(color: _selectedTime == 'total' ? Colors.white : Color(0xFFb5b2d5), fontSize: 17, fontWeight: FontWeight.bold),
                        )),
                    FlatButton(
                        onPressed: () {
                          setState(() => {
                                _selectedTime = 'today',
                              });
                        },
                        child: Text(
                          'Today',
                          style: TextStyle(color: _selectedTime == 'today' ? Colors.white : Color(0xFFb5b2d5), fontSize: 17, fontWeight: FontWeight.bold),
                        )),
                    FlatButton(
                        onPressed: () {
                          setState(() => {
                                _selectedTime = 'yesterday',
                              });
                        },
                        child: Text(
                          'Yesterday',
                          style: TextStyle(color: _selectedTime == 'yesterday' ? Colors.white : Color(0xFFb5b2d5), fontSize: 17, fontWeight: FontWeight.bold),
                        ))
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[_buildInformationBloc(0xFFffb259, 'Affected', infoSpecify.affected, 2), _buildInformationBloc(0xFFff5959, 'Active', infoSpecify.active, 2)],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 17.0, bottom: 50.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    _buildInformationBloc(0xFF4cd97b, 'Recovered', infoSpecify.recovered, 3),
                    _buildInformationBloc(0xFF4cb5ff, 'Death', infoSpecify.death, 3),
                    _buildInformationBloc(0xFF9059ff, 'Serious', infoSpecify.serious, 3)
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  String _containerSelected = '';
  GestureDetector _buildInformationBloc(dynamic color, String title, String info, int nbLine) {
    return GestureDetector(
      onTap: () => {
        setState(() => {_containerSelected = _containerSelected == title ? '' : title})
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        curve: Curves.ease,
        width: width(nbLine, title),
        height: height(nbLine, title),
        padding: EdgeInsets.only(left: 15.0, top: 15.0, bottom: 15.0),
        decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(12)), color: Color(color)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(title, style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold)),
            Text(info, style: TextStyle(color: Colors.white, fontSize: nbLine == 2 ? 24 : 18, fontWeight: FontWeight.bold))
          ],
        ),
      ),
    );
  }

  double width(int nbLine, String title) {
    switch (_containerSelected) {
      case "Active":
        if (title == 'Active') {
          return MediaQuery.of(context).size.width / nbLine - 15;
        } else if (title == 'Affected') {
          return MediaQuery.of(context).size.width / nbLine - 45;
        } else {
          return nbLine == 2 ? MediaQuery.of(context).size.width / nbLine - 35 : MediaQuery.of(context).size.width / nbLine - 28;
        }
        break;
      case "Affected":
        if (title == 'Affected') {
          return MediaQuery.of(context).size.width / nbLine - 15;
        } else if (title == 'Active') {
          return MediaQuery.of(context).size.width / nbLine - 45;
        } else {
          return nbLine == 2 ? MediaQuery.of(context).size.width / nbLine - 35 : MediaQuery.of(context).size.width / nbLine - 28;
        }
        break;
      case "Recovered":
        if (title == 'Recovered') {
          return MediaQuery.of(context).size.width / nbLine - 20;
        } else if (title == 'Death' || title == 'Serious') {
          return MediaQuery.of(context).size.width / nbLine - 35;
        } else {
          return nbLine == 2 ? MediaQuery.of(context).size.width / nbLine - 35 : MediaQuery.of(context).size.width / nbLine - 28;
        }
        break;
      case "Death":
        if (title == 'Death') {
          return MediaQuery.of(context).size.width / nbLine - 20;
        } else if (title == 'Recovered' || title == 'Serious') {
          return MediaQuery.of(context).size.width / nbLine - 35;
        } else {
          return nbLine == 2 ? MediaQuery.of(context).size.width / nbLine - 35 : MediaQuery.of(context).size.width / nbLine - 28;
        }
        break;
      case "Serious":
        if (title == 'Serious') {
          return MediaQuery.of(context).size.width / nbLine - 20;
        } else if (title == 'Recovered' || title == 'Death') {
          return MediaQuery.of(context).size.width / nbLine - 35;
        } else {
          return nbLine == 2 ? MediaQuery.of(context).size.width / nbLine - 35 : MediaQuery.of(context).size.width / nbLine - 28;
        }
        break;
      default:
        return nbLine == 2 ? MediaQuery.of(context).size.width / nbLine - 35 : MediaQuery.of(context).size.width / nbLine - 28;
    }
  }

  double height(int nbLine, String title) {
    if (_containerSelected == 'Active' && title == 'Active') {
      return 120;
    } else if (_containerSelected == 'Affected' && title == 'Affected') {
      return 120;
    } else if (_containerSelected == 'Recovered' && title == 'Recovered') {
      return 120;
    } else if (_containerSelected == 'Death' && title == 'Death') {
      return 120;
    } else if (_containerSelected == 'Serious' && title == 'Serious') {
      return 120;
    } else {
      return 110;
    }
  }
}
