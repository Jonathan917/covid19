import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String dropdownValue = 'CN';

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[_buildHeader(context), _buildPrevention(context)],
        ),
      ),
    );
  }

  Container _buildPrevention(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 25.0, left: 25.0),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 20.0),
            child: Text('Prevention', style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600)),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
            Container(
              width: (MediaQuery.of(context).size.width / 3) - 30,
              child: Column(
                children: <Widget>[
                  Image.asset(
                    './assets/images/distance.png',
                  ),
                  Container(
                    child: Text(
                      'Avoid close contact',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                    ),
                  )
                ],
              ),
            ),
            Container(
                width: (MediaQuery.of(context).size.width / 3) - 30,
                child: Column(
                  children: <Widget>[
                    Image.asset('./assets/images/wash_hands.png'),
                    Container(
                      child: Text(
                        'Clean your hands often',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                      ),
                    )
                  ],
                )),
            Container(
                width: (MediaQuery.of(context).size.width / 3) - 30,
                child: Column(
                  children: <Widget>[
                    Image.asset('./assets/images/mask.png'),
                    Container(
                      child: Text(
                        'Wear a facemask',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
                      ),
                    )
                  ],
                )),
          ]),
          Container(
            margin: EdgeInsets.only(top: 20.0),
            height: 150,
            child: Stack(
              children: <Widget>[
                Positioned(
                  bottom: 0,
                  child: Container(
                    height: 110.0,
                    width: MediaQuery.of(context).size.width - 50,
                    padding: EdgeInsets.only(left: 150.0, right: 20.0, top: 20.0, bottom: 20.0),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(13)),
                        gradient: LinearGradient(begin: Alignment.centerLeft, end: Alignment.centerRight, colors: [Theme.of(context).primaryColorLight, Theme.of(context).primaryColor])),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Do your own test!',
                          style: TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.w700),
                          textAlign: TextAlign.left,
                        ),
                        Text(
                          'Follow the instructions to do your own test.',
                          style: TextStyle(color: Theme.of(context).textSelectionHandleColor, fontSize: 15.0),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                    bottom: 0,
                    left: 10,
                    child: Image.asset(
                      './assets/images/own_test.png',
                      height: 130.0,
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }

  Container _buildHeader(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20.0, bottom: 30.0, right: 25.0, left: 25.0),
      decoration: BoxDecoration(color: Theme.of(context).primaryColor, borderRadius: BorderRadius.only(bottomRight: Radius.circular(50), bottomLeft: Radius.circular(50))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Covid-19',
                  style: TextStyle(color: Colors.white, fontSize: 26.0, fontWeight: FontWeight.w700),
                ),
                Container(
                  padding: EdgeInsets.only(right: 10.0, left: 10.0),
                  decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(25))),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: CircleAvatar(
                          backgroundImage: AssetImage('./assets/images/${dropdownValue.toLowerCase()}_flag.png'),
                          radius: 14,
                        ),
                      ),
                      DropdownButton(
                        value: dropdownValue,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 30,
                        underline: Container(
                          child: null,
                        ),
                        items: <String>['USA', 'FR', 'CN', 'UK', 'IN', 'IT'].map<DropdownMenuItem>((String value) {
                          return DropdownMenuItem<String>(value: value, child: Text(value));
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            dropdownValue = newValue;
                          });
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Text(
              'Are you feeling sick?',
              style: TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.w600),
            ),
          ),
          Text(
            'If you feel sick with any of covid-19 symptoms please call or SMS us immediately for help.',
            style: TextStyle(color: Theme.of(context).textSelectionHandleColor, fontSize: 16, height: 1.5),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 25.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                ButtonTheme(
                  height: 45.0,
                  minWidth: 170.0,
                  child: FlatButton.icon(
                      shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(45.0)),
                      onPressed: () => null,
                      icon: Icon(
                        Icons.phone,
                        color: Colors.white,
                      ),
                      label: Text(
                        'Call Now',
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      ),
                      color: Theme.of(context).accentColor),
                ),
                ButtonTheme(
                  height: 45.0,
                  minWidth: 170.0,
                  child: FlatButton.icon(
                      shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(45.0)),
                      onPressed: () => null,
                      icon: Icon(
                        Icons.chat,
                        color: Colors.white,
                      ),
                      label: Text(
                        'Send SMS',
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      ),
                      color: Theme.of(context).buttonColor),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
