import 'package:covid_app/blocs/status_bloc.dart';
import 'package:covid_app/ui/screens/stats.dart';
import 'package:covid_app/ui/widgets/notification.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'home.dart';

class BottomNavScreen extends StatefulWidget {
  @override
  _BottomNavScreenState createState() => _BottomNavScreenState();
}

class _BottomNavScreenState extends State<BottomNavScreen> {
  final List _screens = [
    HomeScreen(),
    Provider(
      create: (context) => StatusBloc(),
      child: StatsScreen(),
      dispose: (context, value) => value.dispose(),
    ),
    Scaffold(),
    Scaffold(),
    Scaffold(),
  ];
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        drawer: new Drawer(),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Theme.of(context).primaryColor,
          actions: <Widget>[
            NotificationWidget(),
          ],
        ),
        body: _screens[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Colors.white,
            currentIndex: _currentIndex,
            type: BottomNavigationBarType.fixed,
            elevation: 0,
            iconSize: 25.0,
            selectedItemColor: Colors.white,
            unselectedItemColor: Theme.of(context).primaryColorLight,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            onTap: (index) => setState(() => _currentIndex = index),
            items: [Icons.home, Icons.insert_chart, Icons.event_note, Icons.info]
                .asMap()
                .map((key, value) => MapEntry(
                    key,
                    BottomNavigationBarItem(
                        title: Text(''),
                        icon: Container(
                          padding: const EdgeInsets.symmetric(vertical: 6.0, horizontal: 16.0),
                          decoration: BoxDecoration(color: _currentIndex == key ? Theme.of(context).buttonColor : Colors.transparent, borderRadius: BorderRadius.circular(20.0)),
                          child: Icon(value),
                        ))))
                .values
                .toList()));
  }
}
