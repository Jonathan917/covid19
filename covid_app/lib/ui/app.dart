import 'package:covid_app/ui/screens/bottomNav.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: ThemeData(
          primaryColor: Color.fromARGB(255, 71, 63, 151),
          primaryColorLight: Color.fromARGB(255, 153, 159, 191),
          buttonColor: Color.fromARGB(255, 76, 121, 255),
          accentColor: Color.fromARGB(255, 255, 76, 88),
          textSelectionColor: Colors.white,
          textSelectionHandleColor: Color.fromARGB(255, 218, 217, 234)),
      routes: {},
      // AuthBlocProvider(child: RegisterScreen())},
      onUnknownRoute: (RouteSettings setting) {
        return new MaterialPageRoute(builder: (context) => BottomNavScreen());
      },
      home: BottomNavScreen(),
    );
  }
}
