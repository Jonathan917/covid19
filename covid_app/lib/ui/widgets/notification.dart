import 'package:flutter/material.dart';

class NotificationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(
          Icons.notifications_none,
          color: Colors.white,
        ),
        onPressed: null);
  }
}
