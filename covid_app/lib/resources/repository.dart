import 'package:covid_app/model/status.dart';
import 'package:covid_app/resources/covidApi_provider.dart';

class Repository {
  final _covidApiProvider = CovidApiProvider();

  Future<Information> getInfo(String isoCountry) => _covidApiProvider.getInfo(isoCountry);
}
