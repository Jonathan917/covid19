import 'dart:convert';

import 'package:covid_app/model/status.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class CovidApiProvider {
  String _url = "corona.lmao.ninja";
  DateTime now = DateTime.now();

  InformationStatus _todayCountryInfo;
  InformationStatus _totalCountryInfo;
  InformationStatus _yesterdayCountryInfo;
  List<LastCases> _historicalCasesCountry = [];
  InformationStatus _todayGlobalInfo;
  InformationStatus _totalGlobalInfo;
  InformationStatus _yesterdayGlobalInfo;
  List<LastCases> _historicalCasesGlobal = [];

  Future<void> _getInfoCountryTodayTotal(String isoCountry) async {
    Uri uri = new Uri.http(this._url, 'v2/countries/$isoCountry', {"yesterday": "0"});
    Response resp = await http.get(uri);
    dynamic converted = json.decode(resp.body);
    this._todayCountryInfo = new InformationStatus(converted['todayCases'], converted['todayDeaths'], converted['recovered'], converted['active'], converted['critical']);
    this._totalCountryInfo = new InformationStatus(converted['cases'], converted['deaths'], converted['recovered'], converted['active'], converted['critical']);
  }

  Future<void> _getInfoCountryYesterday(String isoCountry) async {
    final uri = new Uri.http(this._url, 'v2/countries/$isoCountry', {"yesterday": "1"});
    final resp = await http.get(uri);
    final converted = json.decode(resp.body);
    this._yesterdayCountryInfo = new InformationStatus(converted['todayCases'], converted['todayDeaths'], converted['recovered'], converted['active'], converted['critical']);
  }

  Future<void> _getHistoricalCountry(String isoCountry) async {
    final uri = new Uri.http(this._url, 'v2/historical/$isoCountry', {"lastdays": "8"});
    final resp = await http.get(uri);
    dynamic decoded = json.decode(resp.body);
    int previouscases = 0;
    this._historicalCasesCountry = [];
    decoded['timeline']['cases'].forEach((k, v) {
      final date = k.split('/');
      this._historicalCasesCountry.add(LastCases(DateTime(int.tryParse(date[2]), int.tryParse(date[0]), int.tryParse(date[1])), v - previouscases));
      previouscases = v;
    });
    this._historicalCasesCountry.removeAt(0);
  }

  Future<void> _getInfoGlobalTodayTotal(String isoCountry) async {
    final uri = new Uri.http(this._url, 'v2/all', {"yesterday": "0"});
    final resp = await http.get(uri);
    final converted = json.decode(resp.body);
    this._todayGlobalInfo = new InformationStatus(converted['todayCases'], converted['todayDeaths'], converted['recovered'], converted['active'], converted['critical']);
    this._totalGlobalInfo = new InformationStatus(converted['cases'], converted['deaths'], converted['recovered'], converted['active'], converted['critical']);
  }

  Future<void> _getInfoGlobalYesterday(String isoCountry) async {
    final uri = new Uri.http(this._url, 'v2/all', {"yesterday": "1"});
    final resp = await http.get(uri);
    final converted = json.decode(resp.body);
    this._yesterdayGlobalInfo = new InformationStatus(converted['todayCases'], converted['todayDeaths'], converted['recovered'], converted['active'], converted['critical']);
  }

  Future<void> _getHistoricalGlobal(String isoCountry) async {
    final uri = new Uri.http(this._url, 'v2/historical/all', {"lastdays": "8"});
    final resp = await http.get(uri);
    final decoded = json.decode(resp.body);
    int previouscases = 0;
    this._historicalCasesGlobal = [];
    decoded['cases'].forEach((k, v) {
      final date = k.split('/');
      this._historicalCasesGlobal.add(LastCases(DateTime(int.tryParse(date[2]), int.tryParse(date[0]), int.tryParse(date[1])), v - previouscases));
      previouscases = v;
    });
    this._historicalCasesGlobal.removeAt(0);
  }

  Future<Information> getInfo(String isoCountry) async {
    try {
      await Future.wait([
        _getInfoCountryTodayTotal(isoCountry),
        _getInfoCountryYesterday(isoCountry),
        _getHistoricalCountry(isoCountry),
        _getInfoGlobalTodayTotal(isoCountry),
        _getInfoGlobalYesterday(isoCountry),
        _getHistoricalGlobal(isoCountry)
      ]);

      return new Information(_todayCountryInfo, _totalCountryInfo, _yesterdayCountryInfo, _historicalCasesCountry, _todayGlobalInfo, _totalGlobalInfo, _yesterdayGlobalInfo, _historicalCasesGlobal);
    } catch (e) {
      print(e);
      return null;
    }
  }
}
