import 'package:intl/intl.dart';

class LastCases {
  DateTime date;
  int count;

  LastCases(this.date, this.count) {}
}

class Information {
  InformationStatus todayCountry;
  InformationStatus totalCountry;
  InformationStatus yesterdayCountry;
  List<LastCases> lastCasesCountry;
  InformationStatus todayGlobal;
  InformationStatus totalGlobal;
  InformationStatus yesterdayGlobal;
  List<LastCases> lastCasesGlobal;

  Information(InformationStatus todayCountry, InformationStatus totalCountry, InformationStatus yesterdayCountry, List<LastCases> lastCasesCountry, InformationStatus todayGlobal,
      InformationStatus totalGlobal, InformationStatus yesterdayGlobal, List<LastCases> lastCasesGlobal) {
    this.todayCountry = todayCountry;
    this.totalCountry = totalCountry;
    this.yesterdayCountry = yesterdayCountry;
    this.lastCasesCountry = lastCasesCountry;
    this.todayGlobal = todayGlobal;
    this.totalGlobal = totalGlobal;
    this.yesterdayGlobal = yesterdayGlobal;
    this.lastCasesGlobal = lastCasesGlobal;
  }
}

class InformationStatus {
  String _affected;
  String _death;
  String _recovered;
  String _active;
  String _serious;

  get affected => _affected;
  get death => _death;
  get recovered => _recovered;
  get active => _active;
  get serious => _serious;

  InformationStatus(int cases, int deaths, int recovered, int active, int critical) {
    final formating = new NumberFormat('###,###,###');
    this._affected = formating.format(cases);
    this._death = formating.format(deaths);
    this._recovered = formating.format(recovered);
    this._active = formating.format(active);
    this._serious = formating.format(critical);
  }
}
